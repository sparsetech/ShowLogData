#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	for (int i = 0; i < 10; i++) {
		ui->listWidget->addItem(QString("some item %1").arg(i));
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

